FROM python:3.8-slim
WORKDIR /backend
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN apt update && apt install -y git
COPY src src
COPY .git .git
ENTRYPOINT ["python3", "src/server.py"]
EXPOSE 9090

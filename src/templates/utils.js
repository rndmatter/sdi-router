var apiURL = `${window.location.protocol}//${window.location.hostname}:${window.location.port}/api/`;
var reloadTimeout = 10000;

function loadResource(accessor, url, reload = false, statusHandler = (status, msg) => {}) {
    $.getJSON(url, (response) => {
        statusHandler(response.status, response.statusMessage);

        resource = accessor();
        response.objects.forEach((d) => {
            index = resource.findIndex((r) => { return r.id == d.id; });
            if (index >= 0 && !resource[index].dirty) {
                resource[index].label = d.label;
                resource[index].locked = d.locked;
            } else if (index < 0) {
                d.dirty = false;
                resource.push(d);
            }
        });
    })
    .done(() => {
        $('#localStatusOverlay').modal('hide');
    })
    .fail(() => {
        $('#localStatusOverlay').modal('setting', 'closable', false).modal('show');
    });

    if (reload) {
        setTimeout(loadResource, reloadTimeout, accessor, url, reload, statusHandler);
    }
}


import asyncio

class AsyncTelnet:
    def __init__(self, retry_delay = 5.0):
        self.retry_delay = retry_delay

        self.listeners = list()
        self._connected = False

    async def _try_connect(self, ip, port):
        try:
            self.reader, self.writer = await asyncio.open_connection(ip, port)
            self._connected = True
            return True
        finally:
            return False

    def connected(self):
        return self._connected

    def register_listener(self, listener):
        self.listeners.append(listener)

    async def connect(self, ip, port):
        await self._try_connect(ip, port)

        while not self.connected():
            # retry after a while until the connection is established
            await asyncio.sleep(self.retry_delay)
            await self._try_connect(ip, port)

        while True:
            if self.reader.at_eof():
                # most likely lost the connection to the endpoint, close the existing connection and retry after a while
                self._connected = False
                self.writer.close()

                await asyncio.sleep(self.retry_delay)
                if not self._try_connect(ip, port):
                    continue

            line = (await self.reader.readline()).decode('utf-8').strip('\n')

            for listener in self.listeners:
                listener.receive(line)

    def send(self, payload):
        if not self.connected:
            return False

        if isinstance(payload, list):
            payload = '{}\n\n'.format('\n'.join(payload))
        elif not isinstance(payload, str):
            raise Exception('unsupported type "" for payload argument'.format(type(payload)))

        self.writer.write(payload.encode('utf-8'))

        return True


import argparse
import asyncio
import json
import pathlib
import random
import re
import telnet

from quart import Quart, redirect, request, render_template, Response

app = Quart(__name__, static_url_path='')
globals = {}
router = None


class generator_op:
    def __init__(self, expr, setter):
        self.expr = expr
        self.setter = setter

    def parse(self, line):
        match = self.expr.match(line)
        if not match:
            app.logger.error('failed to parse line "{}"'.format(line))
            return

        self.setter(match.groups())

lock_expression = re.compile(r'^(\d{1,2}) ([ULO])$')
label_expression = re.compile(r'^(\d{1,2}) (.+)$')
routing_expression = re.compile(r'^(\d{1,2}) (\d{1,2})$')
catch_expression = re.compile(r'(.*)')


class Router:
    def __init__(self, telnet):
        telnet.register_listener(self)
        self.telnet = telnet

        self.output_mappings = {}
        self.output_labels = {}
        self.output_locks = {}
        self.input_labels = {}

        self._op = None
        self.ops = {
            'VIDEO OUTPUT ROUTING': (routing_expression, lambda groups: self.output_mappings.update({int(groups[0]): int(groups[1])})),
            'VIDEO OUTPUT LOCKS': (lock_expression, lambda groups: self.output_locks.update({int(groups[0]): True if groups[1] == 'L' or groups[1] == 'O' else False})),
            'INPUT LABELS': (label_expression, lambda groups: self.input_labels.update({int(groups[0]): groups[1]})),
            'OUTPUT LABELS': (label_expression, lambda groups: self.output_labels.update({int(groups[0]): groups[1]})),
        }

    def status(self):
        if self.telnet.connected():
            return 'connected'

        return 'connecting'

    def statusMessage(self):
        if not self.telnet.connected():
            return 'Backend lost connection to the SDI router'

        return ''

    def _determine_op(self, line):
        op = line.strip(':')
        entry = self.ops.get(op, (catch_expression, lambda x: app.logger.debug('unhandled "{}"'.format(x))))
        self._op = generator_op(*entry)

    def receive(self, line):
        if line == '':
            self._op = None
        elif  self._op is None:
                self._determine_op(line)
        else:
            self._op.parse(line)

    def set_output(self, out_id, in_id):
        self.telnet.send([
            'VIDEO OUTPUT ROUTING:',
            f'{out_id} {in_id}'
        ])

        return {}

    def set_output_label(self, out_id, label):
        self.telnet.send([
            'OUTPUT LABELS:',
            f'{out_id} {label}'
        ])

        return {}

    def set_input_label(self, in_id, label):
        self.telnet.send([
            'INPUT LABELS:',
            f'{in_id} {label}'
        ])

        return {}


class Dummy_Router:
    def __init__(self, num_connections):
        self.output_mappings = {}
        self.output_labels = {}
        self.output_locks = {}
        self.input_labels = {}

        for i in range(0, num_connections):
            self.output_mappings[i] = i
            self.output_labels[i] = 'Dummy Out {:02}'.format(i + 1)
            self.output_locks[i] = i % 3 == 0
            self.input_labels[i] = 'Dummy In {:02}'.format(i + 1)

    def status(self):
        # occasionally simulate a connection issue
        if random.randrange(10) == 0:
            return 'connecting'
        return 'connected'

    def statusMessage(self):
        return 'Backend lost connection to the SDI router'

    def set_output(self, out_id, in_id):
        self.output_mappings[out_id] = in_id

        return {}

    def set_output_label(self, out_id, label):
        self.output_labels[out_id] = label

        return {}

    def set_input_label(self, in_id, label):
        self.input_labels[in_id] = label

        return {}


@app.route('/api/connections')
async def connections():
    # TODO: move this into the router class?
    return json.dumps({
        'objects': [{
            'id': out_id,
            'label': router.output_labels[out_id],
            'locked': router.output_locks[out_id],
            'input': in_id,
        } for out_id, in_id in router.output_mappings.items()],
        'status': router.status(),
        'statusMessage': router.statusMessage(),
    })

@app.route('/api/inputs')
async def inputs():
    return json.dumps({
        'objects': [{
            'id': in_id,
            'label': label,
            'usage': sum([1 for route_input in router.output_mappings.values() if route_input == in_id])
        } for in_id, label in router.input_labels.items()],
        'status': router.status(),
        'statusMessage': router.statusMessage(),
    })

@app.route('/api/route', methods=['POST'])
async def route():
    data = await request.get_json()

    try:
        out_id = int(data['output'])
        in_id = int(data['input'])

        if out_id < 0 or out_id >= len(router.output_mappings):
            raise Exception('expected integer id with [0, {}] for field "ouput"'.format(len(router.output_mappings) - 1))
        if in_id < 0 or in_id >= len(router.input_labels):
            raise Exception('expected integer id with [0, {}] for field "input"'.format(len(router.input_labels) - 1))
    except Exception as e:
        return json.dumps({'error': e})

    return json.dumps(router.set_output(out_id, in_id))

@app.route('/api/changeLabel', methods=['POST'])
async def changeLabel():
    data = await request.get_json()

    try:
        label_id = int(data['id'])
        label_type = str(data['type'])
        label_value = str(data['value'])

        if label_type == 'input':
            if label_id < 0 or label_id >= len(router.input_labels):
                raise Exception('expected integer id with [0, {}] for field "input"'.format(len(router.input_labels) - 1))

            return json.dumps(router.set_input_label(label_id, label_value))

        elif label_type == 'output':
            if label_id < 0 or label_id >= len(router.output_mappings):
                raise Exception('expected integer id with [0, {}] for field "ouput"'.format(len(router.output_mappings) - 1))

            return json.dumps(router.set_output_label(label_id, label_value))

        else:
            raise Exception(f'unexpected type {label_type}')

    except Exception as e:
        return json.dumps({'error': e})

@app.errorhandler(404)
def page_not_found(e):
    return redirect('/')

@app.route('/')
def root():
    return redirect('connections')

@app.route('/labels')
async def labelsHtml():
    return await render_template('labels.html.jinja2', file='labels', **globals)

@app.route('/labels.js')
async def labelsJs():
    return Response(await render_template('labels.js.jinja2'), mimetype='text/javascript')

@app.route('/connections')
async def connectionsHtml():
    return await render_template('connections.html.jinja2', file='connections', **globals)

@app.route('/connections.js')
async def connectionsJs():
    return Response(await render_template('connections.js.jinja2'), mimetype='text/javascript')

def determine_version():
    try:
        parent_dir = pathlib.Path(__file__).parent.parent.absolute()
        print(parent_dir.joinpath('./.git'), parent_dir)
        if parent_dir.joinpath('./.git').exists():
            import git
            repo = git.Repo(parent_dir)

            print(repo)
            globals['hash'] = repo.head.commit.hexsha
            shorthash = repo.head.commit.hexsha[0:7]
            globals['version'] = f'version {shorthash}'
            return
    except:
        pass

    globals['hash'] = 'main'
    globals['version'] = None

if __name__ == '__main__':
    determine_version()

    parser = argparse.ArgumentParser(description='A simple web interface for SDI routers')
    parser.add_argument('--address', type=str, default='localhost', help='IP address of the SDI router')
    parser.add_argument('--port', type=int, default=9990, help='Network port of the SDI router')
    parser.add_argument('--name', type=str, default=None, help='Specify a descriptive name for the router (displayed in the header of the UI)')

    parser.add_argument('--debug', '-d', action='store_true', default=False, help='Run the webserver in debug mode (more verbose and reloads when any Python file is changed)')
    parser.add_argument('--simulated-ports', type=int, default=None, help='Specify number of ports to use to simulate a router. if specified, the backend will not connect to an actual router independent of the other settings.')

    args = parser.parse_args()

    globals['name'] = args.name

    loop = asyncio.get_event_loop()

    if args.simulated_ports is not None:
        globals['name'] = f'Simulated router at {args.address}:{args.port}'
        router = Dummy_Router(args.simulated_ports)
    else:
        tn = telnet.AsyncTelnet()
        router = Router(tn)
        loop.create_task(tn.connect(args.address, args.port))

    if args.debug:
        app.run('0.0.0.0', port=9090, debug=True, loop=loop)
    else:
        import hypercorn

        config = hypercorn.config.Config()
        config.bind = ['0.0.0.0:9090']

        loop.run_until_complete(hypercorn.asyncio.serve(app, config))

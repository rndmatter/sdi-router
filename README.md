# SDI router

A simple web interface for SDI routers from Blackmagicdesign (tested with [Smart Videohub](https://www.blackmagicdesign.com/products/smartvideohub), protocol version 2.8).

## Requirements

- Python 3.7+
- [Quart](https://pgjones.gitlab.io/quart/)

## Running

```bash
# install dependencies if they haven't been installed manually before
pip install -r requirements.txt

# start backend (adjust the IP address based on your setup)
python3 server.py --address 10.0.0.42
```

Alternatively, you can use the provided Dockerfile and/or provide interfaces for multiple routers using [Docker Compose](https://docs.docker.com/compose/compose-file/compose-file-v3/):

```bash
# edit the docker-compose.yml file to provide the --address parameter for your setup
docker-compose build
docker-compose up -d
```

After starting the backend, the UI is available on port 9090.

## Usage

The main screen shows all current connections, that is to say: which SDI output is connected to which SDI input. Greyed-out entries are locked on the SDI switch and can not be changed.

![connections screen](docs/connections.jpg)

After clicking on one of the available outputs, a selection menu opens where you can choose a different input.

![input selection](docs/input_selection.jpg)

In the upper right-hand side of each page, there is a search bar to filter the current view by label. The list of connections (or labels) is updated as you type and case-insensitive. When you are on the connections page, both input- and output labels are considered:

![label filtering](docs/filtered_labels.jpg)

## Testing

You can start the backend with the `--simulated-ports` argument to simulate an SDI router with the specified number of input and output ports (see screenshots above). Note that the backend will occasionally simulate connection issues. This is expected behavior for testing the frontend.

## Limitations

- Only unlocked outputs can be modified. Both, changing the output label and selecting a different input, are not supported for locked outputs.
- Changing the locking state is not supported.
- The backend does not check the software version of the SDI router. Versions different to 2.8 may or may not work.
